package org.wit.mytweet.models;

import android.content.Context;
import android.util.Log;

import org.wit.android.helpers.DbHelper;

import java.util.ArrayList;

public class TweetList {
    //fields for use in model
    public ArrayList<Tweet> tweets;
    public ArrayList<User> users;
    private DbHelper db;


    public TweetList(Context context) {
        try {
            db = new DbHelper(context);
            tweets = (ArrayList<Tweet>)db.selectAllTweets();
            users = new ArrayList<User>();
        }
        catch (Exception e) {
            tweets = new ArrayList<Tweet>();
            users = new ArrayList<User>();
        }
    }

    /**
     * Method to add tweets to existing arraylist.
     *
     * @param tweet
     */
    public void addTweet(Tweet tweet) {
        tweets.add(tweet);
    }

    /**
     * Method to get tweet by tweet id.
     *
     * @param id
     * @return Tweet
     */
    public Tweet getTweet(String id) {
        Log.i(this.getClass().getSimpleName(), "String parameter id: " + id);

        for (Tweet tweet : tweets) {
            if (id.equals(tweet._id)) {
                return tweet;
            }
        }
        return null;
    }


    /**
     * Method called to remove selected tweets in Tweetlistfragment activity.
     *
     * @param tweet
     */
    public void deleteTweet(Tweet tweet){
        if(tweets.contains(tweet)) {
            tweets.remove(tweet);
        }
    }
}
