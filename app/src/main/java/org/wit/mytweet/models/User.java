package org.wit.mytweet.models;

import java.util.ArrayList;

public class User {
    //fields for use in model
    public String _id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public ArrayList<String> follows = new ArrayList<>();
    public int refreshInterval;
    public int numberTweets;



    public User(){
    }
}
