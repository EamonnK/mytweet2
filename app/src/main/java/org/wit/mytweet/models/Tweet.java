package org.wit.mytweet.models;

import android.graphics.Bitmap;
import android.media.Image;
import android.util.Base64;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.sql.Blob;
import java.util.Date;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;


public class Tweet {
    //fields for use in model
    public String tweetMessage;
    public String _id;
    public User user;
    public String createdAt;
    public String userId;
    public String photo;
    public byte[] photoByte;
    public String base64String;
    public Bitmap bitmap;



    /**
     * Constructor for tweet objects.
     */
    public Tweet(){
    }



}
