package org.wit.mytweet.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.Toast;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.wit.android.helpers.IntentHelper.navigateUp;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private SharedPreferences prefs;
    private MyTweetApp app;
    public static final Pattern VALID_NUMBER = Pattern.compile("^[1-9]{1}([0-9]{0,1})?$");


    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        app = MyTweetApp.getApp();
        addPreferencesFromResource(R.xml.settings);
        setHasOptionsMenu(true);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        prefs.registerOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onStop()
    {
        super.onStop();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Method to handle input to preferences, update the users preferences accordingly in the
     * database
     *
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String value = "";
        switch(String.valueOf(key)) {
            case "refresh_interval":
                value = sharedPreferences.getString("refresh_interval",null);
                if(!validate(value)) {
                    Toast toast = Toast.makeText(getActivity(), "Zero or letters not allowed, 99 max!", Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    app.dbHelper.resetInterval(app.thisUser._id, Integer.valueOf(value) * 60000);
                }
                break;

            case "nmr_tweets":
                value = sharedPreferences.getString("nmr_tweets",null);
                if(!validate(value)) {
                    Toast toast = Toast.makeText(getActivity(), "Zero or letters not allowed, 99 max!", Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    app.dbHelper.resetNumberTweets(app.thisUser._id, Integer.valueOf(value));
                }
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                navigateUp(getActivity());
                getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Method to validate input is number only within range of 0-99
     * @param number
     * @return
     */
    public static boolean validate(String number) {
        Matcher matcher = VALID_NUMBER.matcher(number);
        return matcher.find();
    }
}