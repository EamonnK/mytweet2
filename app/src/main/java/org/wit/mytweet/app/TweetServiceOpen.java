package org.wit.mytweet.app;

import java.util.List;

import org.json.JSONObject;
import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TweetServiceOpen {

    @POST("/api/users")
    Call<User> createUser(@Body User User);

    @POST("/api/users/authenticate")
    Call<Token> authenticate(@Body User user);
}
