package org.wit.mytweet.app;


import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.Tweet;
import java.util.List;
import org.wit.mytweet.models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TweetService {
    @GET("/api/users")
    Call<List<User>> getAllUsers();

    @GET("/api/tweets")
    Call<List<Tweet>> getAllTweets();

    @POST("/api/tweets")
    Call<Tweet> createTweet(@Body Tweet tweet);

    @DELETE("/api/tweets/{id}")
    Call<Tweet> deleteOne( @Path ("id") String id);

    @DELETE("/api/users/{id}/tweets")
    Call<Tweet> deleteAll(@Path ("id") String id);

    @POST("/api/users/{id}")
    Call<User> follow(@Path ("id") String id, @Body String idSource);

    @POST("/api/users/unfollow/{id}")
    Call<User> unfollow(@Path ("id") String id, @Body String idSource);
}