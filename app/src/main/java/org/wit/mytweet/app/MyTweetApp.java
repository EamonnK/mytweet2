package org.wit.mytweet.app;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.activities.TweetFragment;
import org.wit.mytweet.activities.TweetListActivity;
import org.wit.mytweet.activities.TweetListFragment;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

public class MyTweetApp extends Application {
    //fields for use in application
    public TweetList tweetList;
    protected static MyTweetApp app;
    public User thisUser;
    public DbHelper dbHelper;
    static final String TAG = "MyTweetApp";
    public TweetService tweetService;
    public TweetServiceOpen tweetServiceOpen;
    public boolean         tweetServiceAvailable = false;
    //public String          service_url  = "http://10.0.2.2:4001";
    public String          service_url  = "https://microblogeamonn.herokuapp.com";
    public int refreshInterval;
    public byte[] byteArray;

    /**
     * Method to initialize variables for use throughout the app.
     */
    @Override
    public void onCreate(){
        super.onCreate();
        dbHelper = new DbHelper(getApplicationContext());
        Log.d(TAG, "MyTweet launched");
        app = this;
        tweetList = new TweetList(getApplicationContext());
        Log.v("TweetPagerActivity", "TweetPagerActivity App Started");
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        tweetServiceOpen = retrofit.create(TweetServiceOpen.class);
        tweetService = RetrofitServiceFactory.createService(TweetService.class);
    }

    /**
     * Getter for call in activities to return MyTweetApp
     *
     * @return MyTweetApp
     */
    public static MyTweetApp getApp(){
        return app;
    }


    /**
     * Method to return service available message
     */
    public void serviceUnavailableMessage() {
        Toast toast = Toast.makeText(this, "MyTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * Method to return service unavailable message
     */
    public void serviceAvailableMessage() {
        Toast toast = Toast.makeText(this, "MyTweet Contacted Successfully", Toast.LENGTH_LONG);
        toast.show();
    }



}
