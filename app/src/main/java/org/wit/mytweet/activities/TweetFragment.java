package org.wit.mytweet.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static org.wit.android.helpers.CameraHelper.showPhoto;
import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;


public class TweetFragment extends Fragment implements TextWatcher, View.OnClickListener, View.OnLongClickListener {
    //fields for use in activity
    public static   final String  EXTRA_TWEET_ID = "mytweet.TWEET_ID";
    private Button tweetButton;
    private Button emailButton;
    private Button contactButton;
    private TextView count;
    private TextView tweeter;
    private EditText editText;
    private Tweet tweet;
    private TextView date;
    private TweetList tweetList;
    private static final int REQUEST_CONTACT = 1;
    private String emailAddress = "";
    MyTweetApp app;
    private ImageView photoView;
    private Intent data;

    /**
     * oncreate method to initialize variables, including selected tweet.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        String tweetId = (String) getArguments().getSerializable(EXTRA_TWEET_ID);
        app = MyTweetApp.getApp();
        tweetList = app.tweetList;
        tweet = tweetList.getTweet(tweetId);
    }

    /**
     * Method to inflate view, utilizing addListneners and updateControls
     * @param inflater
     * @param parent
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);
        TweetPagerActivity myTweet = (TweetPagerActivity)getActivity();
        myTweet.actionBar.setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        return v;
    }

    /**
     * Method to initialize variables, setting content where appropriate.
     *
     * @param v
     */
    private void addListeners(View v){
        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        emailButton = (Button) v.findViewById(R.id.emailButton);
        emailButton.setOnClickListener(this);
        contactButton = (Button) v.findViewById(R.id.contactButton);
        contactButton.setOnClickListener(this);
        count = (TextView) v.findViewById(R.id.count);
        count.setText(String.valueOf(140 - tweet.tweetMessage.length()));
        editText = (EditText) v.findViewById(R.id.editText);
        editText.setText(tweet.tweetMessage);
        editText.setEnabled(false);
        date = (TextView) v.findViewById(R.id.date);
        date.setText(tweet.createdAt.substring(0,9)+"  "+tweet.createdAt.substring(11,19));
        tweeter = (TextView) v.findViewById(R.id.tweeter);
        tweeter.setText(tweet.user.firstName.toUpperCase()+" tweeted...");
        photoView = (ImageView) v.findViewById(R.id.mytweet_imageView);
        if(tweet.photo == null){
            photoView.setVisibility(v.GONE);
        }
        photoView.setOnLongClickListener(this);
    }


    /**
     * onClick listener method for select contact and email buttons
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contactButton :
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;
            case R.id.emailButton :
                sendEmail(getActivity(), emailAddress,"From Twitter", editText.getText().toString());
                contactButton.setText("Select Contact");
                break;
        }
    }

    /**
     * Method to determine result of activity and handle based on resultcode, returning if OK
     * or setting variables for emailing in the case request_contact.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    /**
     * listener method for up button, utilizing navigateUp helper to return to parent.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                navigateUp(getActivity());
                break;
        }
        return true;
    }


    /**
     * Method to listen for long click on image
     *
     * @param v
     * @return
     */
    @Override
    public boolean onLongClick(View v) {
        Intent i = new Intent(getActivity(), TweetGalleryActivity.class);
        i.putExtra(EXTRA_TWEET_ID, tweet._id);
        startActivity(i);
        return true;
    }

    /**
     * Method to show image if the tweet has one
     */
    @Override
    public void onStart() {
        super.onStart();
        if(tweet.photo != null) {
            showPhoto(getActivity(), tweet, photoView);
        }
    }

    /**
     * Method to set email address to button
     */
    public void setContact(){
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        contactButton.setText(name + " : " + emailAddress);
    }

    /**
     * Method to handle permission request for contacts
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setContact();
                }
                break;
            }
        }
    }

    /**
     * Method to check for contact permissions
     *
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            setContact();
        }
        else {
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CONTACT);
        }
    }
}