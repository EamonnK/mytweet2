package org.wit.mytweet.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static org.wit.android.helpers.CameraHelper.showPhoto;
import android.widget.ImageView;

public class TweetActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener, Callback<Tweet> {
    //fields for use in activity
    private Button tweetButton;
    private Button emailButton;
    private Button contactButton;
    private TextView count;
    private EditText editText;
    private Tweet tweet;
    private TextView datetime;
    private Long date;
    private TweetList tweetList;
    private String emailAddress = "";
    private MyTweetApp app;
    private static final int REQUEST_PHOTO = 0;
    private static final int REQUEST_CONTACT = 1;
    private ImageView cameraButton;
    private ImageView photoView;
    private String photo;
    private Intent data;

    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweetactivity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tweetButton = (Button) findViewById(R.id.tweetButton);
        tweetButton.setOnClickListener(this);
        emailButton = (Button) findViewById(R.id.emailButton);
        emailButton.setOnClickListener(this);
        contactButton = (Button) findViewById(R.id.contactButton);
        contactButton.setOnClickListener(this);
        count = (TextView) findViewById(R.id.count);
        editText = (EditText) findViewById(R.id.editText);
        editText.addTextChangedListener(this);
        datetime = (TextView) findViewById(R.id.date);
        date = new Date().getTime();
        datetime.setText(dateString());
        cameraButton = (ImageView) findViewById(R.id.camera_button);
        photoView = (ImageView) findViewById(R.id.mytweet_imageView);
        cameraButton.setOnClickListener(this);
        app = (MyTweetApp) getApplication();
        tweet = new Tweet();
        tweetList = app.tweetList;
    }

    /**
     * Helper method to format date string on view
     *
     * @return String
     */
    public String dateString() {
        String dateFormat = "EEE d MMM yyyy H:mm";
        return android.text.format.DateFormat.format(dateFormat, date).toString();
    }

    /**
     * onClick listener method for tweet, select contact and email buttons
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
             case R.id.tweetButton :
                 checkTweet();
                 break;
            case R.id.contactButton :
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;
            case R.id.emailButton :
                sendEmail(this, emailAddress,"From Twitter", editText.getText().toString());
                contactButton.setText("Select Contact");
                break;
            case R.id.camera_button:
                Intent ic = new Intent(this, TweetCameraActivity.class);
                startActivityForResult(ic, REQUEST_PHOTO);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    /**
     * Listnener method to detect text change, used to adjust counter text on tweetactivity
     *
     * @param s
     */
    @Override
    public void afterTextChanged(Editable s) {
        count.setText(String.valueOf(140 - s.length()));
    }

    /**
     * listener method for up button, utilizing navigateUp helper to return to parent.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:  navigateUp(this);
                finish();
                break;
        }
        return true;
    }

    /**
     * Method to determine result of activity and handle based on resultcode, returning if OK
     * or setting variables for emailing in the case request_contact.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                this.data = data;
                checkContactsReadPermission();
                break;

            case REQUEST_PHOTO:
                String filename = data.getStringExtra(TweetCameraActivity.EXTRA_PHOTO_FILENAME);
                if (filename != null) {
                    tweet.photo = filename;
                    showPhoto(this, tweet, photoView );
                }
                break;
        }

    }

    /**
     * Method called when creating new tweet in onClick, checking if tweet has content and dis-
     * carding if not, otherwise creating tweet, initializing fields and saving to tweetList
     * returning user to tweetlistactivity.
     *
     */
    public void checkTweet(){
        if(this.editText.getText().toString().equals("")){
            Toast toast = Toast.makeText(this, "Tweet field must be filled!", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            tweet = new Tweet();
            tweet.tweetMessage = this.editText.getText().toString();
            tweet.user = app.thisUser;
            if(photo != null) {
                tweet.photo = photo;
            }
            if(app.byteArray != null){
                tweet.photoByte = app.byteArray;
            }
            Call<Tweet> call = (Call<Tweet>) app.tweetService.createTweet(tweet);
            call.enqueue(this);
        }
    }

    /**
     * Method to handle reponse from call adding tweet to local storage
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Tweet> call, Response<Tweet> response) {
        tweet = response.body();
        app.dbHelper.addTweet(tweet);
        Toast toast = Toast.makeText(this, "Tweet sent!", Toast.LENGTH_SHORT);
        toast.show();
        navigateUp(this);
        app.tweetServiceAvailable = true;
        app.byteArray = null;
        finish();
    }


    /**
     * Method to handle failure from call
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Tweet> call, Throwable t) {
        app.tweetServiceAvailable = false;
        app.serviceUnavailableMessage();
        navigateUp(this);
        finish();
    }

    /**
     * Method to navigate up on physical back pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigateUp(this);
        finish();
    }

    /**
     * Method to show picture if one was taken
     */
    @Override
    public void onResume() {
        super.onResume();
        if(tweet.photo != null) {
            photo = tweet.photo;
            showPhoto(this, tweet, photoView);
        }
    }

    /**
     * Method to set found email address to view
     */
    public void setContact(){
        String name = ContactHelper.getContact(this, data);
        emailAddress = ContactHelper.getEmail(this, data);
        contactButton.setText(name + " : " + emailAddress);
    }

    /**
     * Method to handle request for phone permissions result.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CONTACT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setContact();
                }
                break;
            }
        }
    }

    /**
     * Method to check for contacts permissions
     */
    private void checkContactsReadPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            setContact();
        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CONTACT);
        }
    }
}