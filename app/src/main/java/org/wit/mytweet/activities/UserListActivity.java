package org.wit.mytweet.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.User;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wit.android.helpers.IntentHelper.navigateUp;

public class UserListActivity extends AppCompatActivity implements Callback<List<User>>,AdapterView.OnItemClickListener {
    private TweetList tweetList;
    private MyTweetApp app;
    private ListView listView;
    private UserAdapter adapter;

    /**
     * Method to initialize user list view
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_userlist);
        listView = (ListView) findViewById(R.id.userList);
        app = MyTweetApp.getApp();
        tweetList = app.tweetList;
        adapter = new UserAdapter(this, tweetList.users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        //call for list of users for follows/unfollow view
        Call<List<User>> call = app.tweetService.getAllUsers();
        call.enqueue(this);
    }

    /**
     * Method to handle response from call
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
        app.tweetServiceAvailable = true;
        tweetList.users.clear();
        for(User u: response.body()) {
            if(!u._id.equals(app.thisUser._id)) {
                tweetList.users.add(u);
            }else{
                app.thisUser = u;
            }
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * Method to handle call failure
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<List<User>> call, Throwable t) {
        app.tweetServiceAvailable = false;
        app.serviceUnavailableMessage();
    }


    /**
     * Method to handle up button pressed
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                startActivity(new Intent(this, TweetListActivity.class));
                finish();
                break;
        }
        return true;
    }


    /**
     * Method to call for follows or unfollows relationship, handling response and failure
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = adapter.getItem(position);
        if (!app.thisUser.follows.contains(user._id)) {
            Call<User> call = app.tweetService.follow(user._id, app.thisUser._id);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    app.tweetServiceAvailable = true;
                    startActivity(new Intent(UserListActivity.this, UserListActivity.class));
                    finish();
                    Toast toast = Toast.makeText(UserListActivity.this, "Followed !", Toast.LENGTH_SHORT);
                    toast.show();
                }
                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    app.tweetServiceAvailable = false;
                    Toast toast = Toast.makeText(UserListActivity.this, "Unavailable, try again later!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        } else if (app.thisUser.follows.contains(user._id)) {
            Call<User> call = app.tweetService.unfollow(user._id, app.thisUser._id);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    app.tweetServiceAvailable = true;
                    startActivity(new Intent(UserListActivity.this, UserListActivity.class));
                    finish();
                    Toast toast = Toast.makeText(UserListActivity.this, "Unfollowed !", Toast.LENGTH_SHORT);
                    toast.show();
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    app.tweetServiceAvailable = false;
                    Toast toast = Toast.makeText(UserListActivity.this, "Unavailable, try again later!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });
        }
    }

    /**
     * Method to navigate up on physical back pressed
     *
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigateUp(this);
        finish();
    }

}

class UserAdapter extends ArrayAdapter<User> {
    private Context context;
    private MyTweetApp app = MyTweetApp.getApp();;

    /**
     * Constructor for useradapter
     *
     * @param context
     * @param users
     */
    public UserAdapter(Context context, ArrayList<User> users) {
        super(context, 0, users);
        this.context = context;
    }

    /**
     * Method to get user list items and initialize their fields
     *
     * @param position
     * @param convertView
     * @param parent
     * @return convertView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.list_item_user, null);
        }
        User user = getItem(position);

        TextView tweet_list_user= (TextView) convertView.findViewById(R.id.user_list_user);
        tweet_list_user.setText(user.firstName + " "+ user.lastName);

        TextView foll= (TextView) convertView.findViewById(R.id.userButton);


        if(app.thisUser.follows.contains(user._id)){
            foll.setText("unfollow");
        }else{
            foll.setText("follow");
        }

        return convertView;

    }
}
