package org.wit.mytweet.activities;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView;
import android.view.ActionMode;
import android.widget.Toast;
import org.wit.android.helpers.DbHelper;
import org.wit.android.helpers.SortByDate;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.settings.SettingsActivity;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static org.wit.android.helpers.FileIOHelper.writeBitmap;
import static org.wit.android.helpers.IntentHelper.startActivityWithData;

public class TweetListFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener, Callback<Tweet> {
    //fields for use in activity
    private ArrayList<Tweet> tweets;
    private ListView listView;
    private TweetList tweetList;
    private TweetAdapter adapter;
    private Tweet tweet;
    private DbHelper dbHelper;
    private Timer timer;
    private ArrayList<Tweet> delTweets = new ArrayList<>();
    MyTweetApp app;
    public static   final String  EXTRA_PHOTO_FILENAME = "org.wit.mytweet.photo.filename";

    /**
     * oncreate method to initialize variables.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);
        app = MyTweetApp.getApp();
        dbHelper = DbHelper.getInstance(getActivity());
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                refreshTweets();
            }
        },0, dbHelper.getInterval(app.thisUser._id));
        tweetList = app.tweetList;
        tweets = tweetList.tweets;
        adapter = new TweetAdapter(getActivity(), tweets);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /**
     * Method to call for list of tweets, handling response or failure, allowing also for pulled
     * images from web application.
     */
    public void refreshTweets() {
        Call<List<Tweet>> call2 = (Call<List<Tweet>>) app.tweetService.getAllTweets();
        call2.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                tweetList.tweets.clear();
                dbHelper.deleteTweets();
                Collections.sort(response.body(), new SortByDate());
                for(Tweet t : response.body()){
                    if(app.thisUser._id.equals(t.user._id) || app.thisUser.follows.contains(t.user._id)) {
                        t.userId = t.user._id;
                        if(t.base64String != null) {
                            byte[] decodedString = Base64.decode(t.base64String, Base64.DEFAULT);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            t.bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            t.bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            if(t.photo == null) {
                                t.photo = UUID.randomUUID().toString() + ".png";
                                if (writeBitmap(getActivity(), t.photo, t.bitmap)) {
                                    Intent intent = new Intent();
                                    intent.putExtra(EXTRA_PHOTO_FILENAME, t.photo);
                                    getActivity().setResult(Activity.RESULT_OK, intent);
                                }
                            }
                        }
                        tweetList.addTweet(t);
                        dbHelper.addTweet(t);
                        if(tweetList.tweets.size() == dbHelper.getNumberTweets(app.thisUser._id)){
                            break;
                        }
                    }
                }
                adapter.notifyDataSetChanged();
                app.tweetServiceAvailable = true;
                app.serviceAvailableMessage();

            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Log.v("error",String.valueOf(t));
                app.tweetServiceAvailable = false;
                tweetList.tweets = (ArrayList<Tweet>) dbHelper.selectAllTweets();
                app.serviceUnavailableMessage();
            }
        });
    }

    /**
     * Method to inflate view initializing listview and View v
     *
     * @param inflater
     * @param parent
     * @param savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        adapter.notifyDataSetChanged();
        return v;
    }

    /**
     * Listener method to get position of tweet in adapter and start activity with appropriate
     * data.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tweet tweet = adapter.getItem(position);
        startActivityWithData(getActivity(), TweetPagerActivity.class, "TWEET_ID", tweet._id);
        stopTimer();

    }

    /**
     * method to inflate options menu.
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweetlist, menu);
    }

    /**
     * Listener method to determine which menu option has been selected and respond with
     * appropriate action.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_tweet:
                 if(!app.tweetServiceAvailable){
                    Toast toast =  Toast.makeText(getActivity(), "Service unavailable, please try later...", Toast.LENGTH_SHORT);
                    toast.show();
                 }else {
                    startActivity(new Intent(getActivity(), TweetActivity.class));
                    stopTimer();
                }
                break;

            case R.id.menu_item_refresh:
                 if(!app.tweetServiceAvailable){
                    Toast toast =  Toast.makeText(getActivity(), "Service unavailable, please try later...", Toast.LENGTH_SHORT);
                    toast.show();
                 }else {
                    refreshTweets();
                }
                break;

            case R.id.clear:
                if (app.tweetServiceAvailable){
                     for(Tweet t : tweetList.tweets){
                         if(t.userId.equals(app.thisUser._id)) {
                            delTweets.add(t);
                        }
                    }
                    Call<Tweet> call = app.tweetService.deleteAll(app.thisUser._id);
                    call.enqueue(this);
                } else {
                    Toast toast = Toast.makeText(getActivity(), "Service unavailable, please try later...", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;

            case R.id.action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                stopTimer();
                break;

            case R.id.users:
                if(!app.tweetServiceAvailable){
                    Toast toast =  Toast.makeText(getActivity(), "Service unavailable, please try later...", Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    startActivity(new Intent(getActivity(), UserListActivity.class));
                    stopTimer();
                }
                break;

            case R.id.welcome:
                startActivity(new Intent(getActivity(), Welcome.class));
                stopTimer();
        }
        return true;
    }

    /**
     * Method to cancel timer once activity is switched
     */
    public void stopTimer(){
        timer.cancel();
        timer.purge();
        timer = null;
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

    }

    /**
     * Method to inflate tweetlist delete menu.
     *
     * @param mode
     * @param menu
     * @return boolean
     */
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.tweet_list_context, menu);
        return true;
    }


    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    /**
     * Method to listen for item click, entering delte mode on long hold.
     *
     * @param mode
     * @param item
     * @return boolean
     */
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete_tweet:
                deleteTweet(mode);
                return true;
            default:
                return false;
        }
    }

    /**
     * Method called to delete selected items from tweetlist.
     *
     * @param actionMode
     */
    private void deleteTweet(ActionMode actionMode) {
        for (int i = adapter.getCount() - 1; i >= 0; i--) {

            if (listView.isItemChecked(i)) {
                tweet = adapter.getItem(i);
                if(tweet.userId.equals(app.thisUser._id)) {
                    delTweets.add(adapter.getItem(i));
                    Call<Tweet> call = app.tweetService.deleteOne(tweet._id);
                    call.enqueue(this);
                } else {
                    Toast toast = Toast.makeText(getActivity(), "Only your tweets can you delete!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

        }
        actionMode.finish();

    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }


    /**
     * Listener for list item clicked, getting item by position, starting tweetpageractivity
     * and adding tweetFragment and tweetId extras.
     *
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tweet = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetPagerActivity.class);
        i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet._id);
        startActivityForResult(i, 0);
        stopTimer();
    }

    /**
     * Method to notify TweetAdapter of change in data-set on resumption of TweetListFragment
     */
    @Override
    public void onResume() {
        super.onResume();
        ((TweetAdapter)getListAdapter()).notifyDataSetChanged();
    }


    /**
     * Method to handle response from delete user tweets
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Tweet> call, Response<Tweet> response) {
        app.tweetServiceAvailable = true;
        for(Tweet t : delTweets) {
            tweetList.deleteTweet(t);
            app.dbHelper.deleteTweet(t._id);
        }
        delTweets.clear();
        adapter.notifyDataSetChanged();
        Log.v("response: ", String.valueOf(response.body()));
        Toast toast =  Toast.makeText(getActivity(), "Success, your tweets have been removed!", Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Method to handle failure from call to delete tweets
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Tweet> call, Throwable t) {
        app.tweetServiceAvailable = false;
        app.serviceUnavailableMessage();
    }
}

class TweetAdapter extends ArrayAdapter<Tweet> {
    private Context context;

    /**
     * Constructor for tweetadapter
     *
     * @param context
     * @param tweets
     */
    public TweetAdapter(Context context, ArrayList<Tweet> tweets) {
        super(context, 0, tweets);
        this.context = context;
    }

    /**
     * Method to get tweet list items and initialize their fields, user, message and date.
     *
     * @param position
     * @param convertView
     * @param parent
     * @return convertView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet tweet = getItem(position);

        TextView tweet_list_user= (TextView) convertView.findViewById(R.id.tweet_list_user);
        tweet_list_user.setText(tweet.user.firstName + " tweets...");

        TextView tweet_list_item = (TextView) convertView.findViewById(R.id.tweet_list_item);
        tweet_list_item.setText(tweet.tweetMessage);

        TextView tweet_list_item_date = (TextView) convertView.findViewById(R.id.tweet_list_item_date);
        tweet_list_item_date.setText(tweet.createdAt.substring(0,9)+"  "+tweet.createdAt.substring(11,19));


        return convertView;
    }
}
