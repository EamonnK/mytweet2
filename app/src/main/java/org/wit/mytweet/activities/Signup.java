package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;
import org.wit.mytweet.R;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity implements Callback<User>{
    //fields for use in activity
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private MyTweetApp app;
    private DbHelper db;
    public static final Pattern VALID_EMAIL_REG = Pattern.compile("^[A-Z0-9_]+@[A-Z0-9]+\\.[A-Z]{1,}$", Pattern.CASE_INSENSITIVE);

    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
        firstName = (EditText) findViewById(R.id.firstName);
        firstName.setText("");
        lastName = (EditText) findViewById(R.id.lastName);
        lastName.setText("");
        email = (EditText) findViewById(R.id.email);
        email.setText("");
        password = (EditText) findViewById(R.id.password);
        db = DbHelper.getInstance(this);
    }

    /**
     * Method to register user, taking input variables, ensuring all fields are filled, checking
     * against the database to make sure email doesnt already exist, replying with appropriate toast
     * if email already exists.
     *
     * Now validates email, implements call for registration
     *
     * @param view
     */
    public void registerUser(View view){
        String firstName = this.firstName.getText().toString();
        String lastName  = this.lastName.getText().toString();
        String email     = this.email.getText().toString();
        String password  = this.password.getText().toString();
        if(db.checkEmail(email)){
            Toast toast = Toast.makeText(this, "Email is already in our database.", Toast.LENGTH_SHORT);
            this.email.setText("");
            toast.show();
        }
        else if(firstName.equals("") || lastName.equals("") || email.equals("") || password.equals("") ) {
            Toast toast = Toast.makeText(this, "All fields must be filled", Toast.LENGTH_SHORT);
            toast.show();
        }else if(!validate(email)) {
            Toast toast = Toast.makeText(this, "Email must be valid", Toast.LENGTH_SHORT);
            this.email.setText("");
            toast.show();
        }else{
            User user = new User();
            user.firstName = firstName;
            user.lastName = lastName;
            user.email = email;
            user.password = password;
            Call<User> call = (Call<User>) app.tweetServiceOpen.createUser(user);
            call.enqueue(this);
        }
    }

    /**
     * Method to listen for pressing of the hardware back button.
     * if pressed calling onbackpressed().
     *
     * @param keyCode
     * @param event
     * @return boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Method to redirect user to welcome activity when back button is pressed,
     * finishing the current activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
        finish();
    }

    /**
     * Method to handle respnse from call
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        app.tweetServiceAvailable = true;
        if(db.returnUser(response.body().email, response.body().password) == null){
            db.addUser(response.body());
        }
        startActivity(new Intent(this, Login.class));
        finish();
    }

    /**
     * Method to handle failure from call
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<User> call, Throwable t) {
        app.tweetServiceAvailable = false;
        app.serviceUnavailableMessage();
    }

    /**
     * Method to validate email
     *
     * @param email
     * @return boolean
     */
    public static boolean validate(String email) {
        Matcher matcher = VALID_EMAIL_REG.matcher(email);
        return matcher.find();
    }
}
