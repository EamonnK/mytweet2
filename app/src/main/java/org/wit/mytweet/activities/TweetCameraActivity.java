package org.wit.mytweet.activities;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.util.UUID;
import static org.wit.android.helpers.FileIOHelper.writeBitmap;
import static org.wit.android.helpers.IntentHelper.navigateUp;

public class TweetCameraActivity extends AppCompatActivity implements View.OnClickListener {

    private Button savePhoto;
    private Button takePhoto;
    private ImageView tweetImage;
    public static final int CAMERA_PERMISSION = 0;
    private static  final int CAMERA_RESULT = 5;
    public static   final String  EXTRA_PHOTO_FILENAME = "org.wit.mytweet.photo.filename";
    private Bitmap tweetPhoto;
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweet_photo);
        app = MyTweetApp.getApp();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tweetImage      = (ImageView) findViewById(R.id.tweetImage);
        savePhoto       = (Button)findViewById(R.id.savePhoto);
        takePhoto       = (Button)findViewById(R.id.takePhoto);
        savePhoto.setEnabled(false);
        savePhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
    }


    /**
     * Method to handle up button pressed
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateUp(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method to listen for click event and handle request
     *
     * @param v
     */
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.takePhoto     :
                checkCameraPermission();
                break;

            case R.id.savePhoto     : onPictureTaken(tweetPhoto);
                break;
        }
    }

    /**
     * Method to initialize camera on request
     */
    public void onTakePhotoClicked() {
        // Check for presence of device camera. If not present advise user and quit method.
        PackageManager pm = getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "Camera app not present on this device", Toast.LENGTH_SHORT).show();
            return;
        }
        // The device has a camera app ... so use it.
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent,CAMERA_RESULT);
        savePhoto.setEnabled(true);
    }

    /**
     * Method to format and store picture once taken
     *
     * @param data
     */
    public void onPictureTaken(Bitmap data) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        data.compress(Bitmap.CompressFormat.PNG, 100, stream);
        app.byteArray = stream.toByteArray();
        String filename = UUID.randomUUID().toString() + ".png";
        if (writeBitmap(this, filename, data) == true) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_PHOTO_FILENAME, filename);
            setResult(Activity.RESULT_OK, intent);
        }
        else {
            setResult(Activity.RESULT_CANCELED);
        }
        finish();
    }

    /**
     * Method to handle result of camera activity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TweetCameraActivity.CAMERA_RESULT :
                if(data != null) {
                    processImage(data);
                }
                else {
                    Toast.makeText(this, "Camera failure: check simulated camera present emulator advanced settings",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /**
     * Method to process image utilized in activity result
     *
     * @param data
     */
    private void processImage(Intent data) {
        tweetPhoto = (Bitmap) data.getExtras().get("data");
        if(tweetPhoto == null) {
            Toast.makeText(this, "Attempt to take photo did not succeed", Toast.LENGTH_SHORT).show();
        }
        tweetImage.setImageBitmap(tweetPhoto);
    }

    /**
     * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
     * This is an override of AppCompat.onRequestPermissionsResult
     *
     * @param requestCode
     * @param permissions String array of permissions requested.
     * @param grantResults int array of results for permissions request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case TweetCameraActivity.CAMERA_RESULT : {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onTakePhotoClicked();
                }
                break;
            }
        }
    }

    /**
     * Bespoke method to check if camera permission exists.
     * If it exists then the photo taken.
     * Otherwise, the method AppCompat.request permissions is invoked and
     * The response is via the callback onRequestPermissionsResult.
     * In onRequestPermissionsResult, on successfully being granted permission photo taken.
     * See: https://developer.android.com/training/permissions/requesting.html
     * See: http://bit.ly/2hBUAZD
     */
    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            onTakePhotoClicked();
        }
        else {
            ActivityCompat.requestPermissions(TweetCameraActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    CAMERA_PERMISSION);
        }
    }
}
