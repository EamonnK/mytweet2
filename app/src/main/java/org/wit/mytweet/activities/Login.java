package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.app.RetrofitServiceFactory;
import org.wit.mytweet.app.TweetService;
import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements Callback<Token>{
    //fields for use in activity
    private EditText email;
    private EditText password;
    private MyTweetApp app;
    private DbHelper db;


    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        app = (MyTweetApp) getApplication();
        email        = (EditText) findViewById(R.id.email);
        email.setText("");
        password     = (EditText) findViewById(R.id.password);
        db = DbHelper.getInstance(this);
    }

    /**
     * Method to login user, validating the input details and checking against
     * the database, responding with toast if details are invalid or on success
     * starting tweetlistactivity
     *
     * @param view
     */
    public void login(View view){
        String email = this.email.getText().toString();
        String password = this.password.getText().toString();
        validUser(email, password);
    }

    /**
     * Method to listen for pressing of the hardware back button.
     * if pressed calling onbackpressed().
     *
     * @param keyCode
     * @param event
     * @return boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Method to redirect user to welcome activity when back button is pressed,
     * finishing the current activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
        finish();
    }

    /**
     * Method to validate user on login
     *
     * @param email
     * @param password
     */
    public void validUser (String email, String password)
    {
        User user = new User();
        user.email = email;
        user.password = password;
        app.tweetServiceOpen.authenticate(user);
        Call<Token> call = (Call<Token>) app.tweetServiceOpen.authenticate (user);
        call.enqueue(this);
    }

    /**
     * Method to handle response from call for login
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Token> call, Response<Token> response) {
        if(response.body().success) {
            app.thisUser = response.body().user;
            Token toke = response.body();
            app.tweetService = RetrofitServiceFactory.createService(TweetService.class, toke.token);
            app.tweetServiceAvailable = true;
            if(!db.findUser(app.thisUser.email, app.thisUser.password)) {
                response.body().user.refreshInterval = 10000;
                response.body().user.numberTweets = 10;
                db.addUser(response.body().user);
            }
            startActivity(new Intent(this, TweetListActivity.class));
            finish();
        } else {
            this.email.setText("");
            this.password.setText("");
            Toast toast =  Toast.makeText(this, "Incorrect login credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Method to handle failure from call for login
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Token> call, Throwable t) {
        String email = this.email.getText().toString();
        String password = this.password.getText().toString();
        if(db.findUser(email, password)){
            app.tweetServiceAvailable = false;
            app.serviceUnavailableMessage();
            app.thisUser = db.returnUser(email, password);
            startActivity(new Intent(this, TweetListActivity.class));
            finish();
        }else{
            this.email.setText("");
            this.password.setText("");
            Toast toast =  Toast.makeText(this, "Incorrect login credentials", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
