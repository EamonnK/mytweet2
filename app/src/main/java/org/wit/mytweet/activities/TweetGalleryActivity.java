package org.wit.mytweet.activities;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.Tweet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import static org.wit.android.helpers.CameraHelper.showPhoto;

public class TweetGalleryActivity extends AppCompatActivity {

    private ImageView photoView;

    /**
     * Method to  initialize gallery view
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweet_gallery);
        photoView = (ImageView) findViewById(R.id.tweetGalleryImage);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showPicture();
    }

    /**
     * Method to listen for up button pressed
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home  : onBackPressed();
                return true;
            default                 : return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method to show picture
     */
    private void showPicture() {
        String tweetId = (String)getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
        MyTweetApp app = (MyTweetApp) getApplication();
        TweetList tweetList = app.tweetList;
        Tweet tweet = tweetList.getTweet(tweetId);
        showPhoto(this, tweet,  photoView);
    }
}
