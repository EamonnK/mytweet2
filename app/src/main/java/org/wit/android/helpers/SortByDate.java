package org.wit.android.helpers;

import java.util.Comparator;
import org.wit.mytweet.models.Tweet;

public class SortByDate implements Comparator<Tweet>{

    /**
     * Comparator to sort objects of tweet model by date.
     */
    @Override
    public int compare(Tweet a, Tweet b) {
        return b.createdAt.compareTo(a.createdAt);
    }

}