package org.wit.android.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;
import java.util.ArrayList;
import java.util.List;


public class DbHelper extends SQLiteOpenHelper {

    static final String TAG = "DbHelper";
    static final String DATABASE_NAME = "mytweet.db";
    static final int DATABASE_VERSION = 1;
    static final String TABLE_USERS = "tableUsers";
    static final String PRIMARY_KEY = "id";
    static final String FIRSTNAME = "firstName";
    static final String LASTNAME = "lastName";
    static final String EMAIL = "email";
    static final String PASSWORD = "password";
    static final String TABLE_TWEETS = "tableTweets";
    static final String TWEETMESSAGE = "tweetMessage";
    static final String USER = "user";
    static final String CREATEDAT = "createdAt";
    static final String USERID = "userId";
    static final String REFRESHINTERVAL = "refreshInterval";
    static final String NUMBERTWEETS = "numberTweets";
    static final String PHOTO = "photo";

    private static DbHelper sInstance;

    Context context;

    /**
     *
     * @param context
     */
    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * onCreate method taking db as param, creating table of users.
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createUserTable =
                "CREATE TABLE tableUsers " +
                        "(id TEXT PRIMARY_KEY, " +
                        "firstName TEXT, " +
                        "lastName TEXT, " +
                        "email TEXT, " +
                        "password TEXT, " +
                        "refreshInterval INTEGER, " +
                        "numberTweets INTEGER)";

        db.execSQL(createUserTable);
        String createTweetTable =
                "CREATE TABLE tableTweets " +
                        "(id TEXT PRIMARY_KEY, " +
                        "tweetMessage TEXT, " +
                        "user TEXT, " +
                        "createdAt TEXT, " +
                        "userId TEXT, " +
                        "photo TEXT)";

        db.execSQL(createTweetTable);
        Log.d(TAG, "DbHelper.onCreated: " + createUserTable+ " "+createTweetTable);
    }


    /**
     * Method to insert user entries to table.
     *
     * @param user
     */
    public void addUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, user._id);
        values.put(FIRSTNAME, user.firstName);
        values.put(LASTNAME, user.lastName);
        values.put(EMAIL, user.email);
        values.put(PASSWORD, user.password);
        values.put(REFRESHINTERVAL, user.refreshInterval);
        values.put(NUMBERTWEETS, user.numberTweets);
        // Insert record
        db.insert(TABLE_USERS, null, values);
        db.close();
    }

    /**
     * Method to insert user entries to table.
     *
     * @param tweet
     */
    public void addTweet(Tweet tweet){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, tweet._id);
        values.put(TWEETMESSAGE, tweet.tweetMessage);
        values.put(USER, tweet.user.firstName);
        values.put(CREATEDAT, tweet.createdAt);
        values.put(USERID, tweet.userId);
        values.put(PHOTO, tweet.photo);
        // Insert record
        db.insert(TABLE_TWEETS, null, values);
        db.close();
    }


    /**
     * Method to delete tweet
     *
     * @param id
     */
    public void deleteTweet(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("tableTweets", "id" + "=?", new String[]{id + ""});
        }
        catch (Exception e) {
            Log.d(TAG, "delete tweet failure: " + e.getMessage());
        }
    }

    /**
     * Method to return list of all tweets from the database
     *
     * @return List tweet
     */
    public List<Tweet> selectAllTweets() {
        List<Tweet> tweets = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM tableTweets",null);
        if (cursor.moveToFirst()) {
            int columnIndex = 0;
            do {
                Tweet tweet = new Tweet();
                tweet._id = cursor.getString(columnIndex++);
                tweet.tweetMessage = cursor.getString(columnIndex++);
                tweet.user = new User();
                tweet.user.firstName = cursor.getString(columnIndex++);
                tweet.createdAt = cursor.getString(columnIndex++);
                tweet.userId = cursor.getString(columnIndex++);
                tweet.photo = cursor.getString(columnIndex++);
                columnIndex = 0;
                tweets.add(tweet);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tweets;
    }

    /**
     * Method to delete all tweets
     */
    public void deleteTweets() {
        SQLiteDatabase db =  this.getWritableDatabase();
        db.execSQL("DELETE FROM tableTweets");
    }


    /**
     *Method called when app is upgraded, launched and the database is not the same.
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("drop table if exists " + TABLE_USERS);
        db.execSQL("drop table if exists " + TABLE_TWEETS);
        Log.d(TAG, "onUpdated");
        onCreate(db);
    }

    /**
     * Method for calling db in activities setting appropriate context.
     *
     * @param context
     * @return
     */
    public static synchronized DbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Helper method to find user in database, utilized in login to ensure user credentials are
     * matching and correct.
     *
     * @param useremail
     * @param userpassword
     * @return boolean
     */
    public boolean findUser(String useremail, String userpassword) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * from tableUsers WHERE email = " +"'"+ useremail+"'"+" AND password = " + "'"+userpassword+"'", null);
        if(cur.getCount() == 1){
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    /**
     * method to find and return a user
     *
     * @param email
     * @param password
     * @return User
     */
    public User returnUser(String email, String password) {
        User user;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            user = new User();
            cursor = db.rawQuery("SELECT * from tableUsers WHERE email = " +"'"+ email+"'"+" AND password = " + "'"+password+"'", null);
            if (cursor.getCount() > 0) {
                int columnIndex = 0;
                cursor.moveToFirst();
                user._id = cursor.getString(columnIndex++);
                user.firstName = cursor.getString(columnIndex++);
                user.lastName = cursor.getString(columnIndex++);
                user.email = cursor.getString(columnIndex++);
                user.password = cursor.getString(columnIndex++);
            }
        } finally {
            cursor.close();
        }
        return user;
    }

    /**
     * Helper method to check if email exists in database when a user is registering, utilized in
     * signup.
     *
     * @param useremail
     * @return boolean
     */
    public boolean checkEmail(String useremail) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT email from tableUsers WHERE email = " +"'"+ useremail+"'", null);
        if(cur.getCount() == 1){
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    /**
     * Method to reset the tweet refresh interval from the settings view
     *
     * @param id
     * @param number
     */
    public void resetInterval(String id, int number){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        Log.v("hitting this", "here1"+String.valueOf(number));
        String update = "UPDATE tableUsers SET refreshInterval=" + String.valueOf(number) +" WHERE id = '" +id+"';";
        Log.v("hitting this", "here2");
        db.execSQL(update);
        Log.v("hitting this", "here3");
    }

    /**
     * method to reset the number of tweets pulled
     *
     * @param id
     * @param number
     */
    public void resetNumberTweets(String id, int number){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        String update ="UPDATE tableUsers SET numberTweets=" + String.valueOf(number) +" WHERE id = '" +id+"';";
        db.execSQL(update);
    }

    /**
     * method to return a users refresh interval
     *
     * @param id
     * @return int
     */
    public int getInterval(String id){
        SQLiteDatabase db = sInstance.getReadableDatabase();
        int interval = 0;
        Cursor cursor = db.rawQuery("SELECT * FROM tableUsers WHERE id = '" + id + "'", null);
        if(cursor.moveToFirst()) {
            interval = cursor.getInt(5);
        }
        cursor.close();
        return interval;
    }

    /**
     * method to return a users tweet limit number
     *
     * @param id
     * @return int
     */
    public int getNumberTweets(String id) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        int numberTweets = 0;
        Cursor cursor = db.rawQuery("SELECT * FROM tableUsers WHERE id = '" + id + "'", null);
        if(cursor.moveToFirst()) {
            numberTweets = cursor.getInt(6);
        }
        cursor.close();
        return numberTweets;
    }
}




