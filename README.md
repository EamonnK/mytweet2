#MyTweet2, Java#

### What is this repository ###

* This repository contains a twitter clone application
* Users can sign up/login, make tweets with images or without.
* Establish social relationships from userlist view.
* Delete selected or all tweets made by them.
* Edit their own account settings, ie, Tweet pull limit.
* Filter their timeline view with follow/unfollow relationship
* Maintains a cache of tweets locally to allow for offline login once registered

### How to run ###

* Open using Android Studio
* Run on emulator or android device
* login: homer@simpson.com, password: secret